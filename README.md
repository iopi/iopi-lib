# IoPi-Bibliothek
Die IoPi-Bibliothek ist eine Bibliothek für die Arduino IDE (https://www.arduino.cc/) ab Version 1.6.10. Sie setzt auf der HttpClient-Bibliothek auf (https://www.arduino.cc/en/Tutorial/HttpClient) und abstrahiert die Kommunikation mit einem IoPi-Server (https://gitlab.informatik.hu-berlin.de/iopi/iopi-srv).

## Installation
Laden Sie die aktuelle Version der Bibliothek herunter: https://gitlab.informatik.hu-berlin.de/iopi/iopi-lib/tags 
Danach können Sie die Bibliothek in der Arduino IDE über `Sketch > Bibliothek einbinden > .ZIP-Bibliothek hinzufügen...` installieren. Weitere Informationen finden Sie unter https://www.arduino.cc/en/Guide/Libraries

## Benutzung
Die Bibliothek stellt die Klasse `IoPiServer` zur Verfügung, welche die im Folgenden aufgeführten Methoden zur Verfügung stellt. Beispiele finden sich im Ordner `IoPi/examples`.

### void `IoPiServer`::`begin`(`server`, `projekt`)
Die Methode initialisiert die Objektinstanz. Sie sollte in der Setup-Funktion aufgerufen werden. Sie erhält zwei Parameter:
- `server` die Adresse des IoPi-Servers, z.B. "http://iopi.informatik.hu-berlin.de" oder "192.168.5.95:8080"
- `projekt` der Name des entsprechenden Projektes auf dem Server, z.B. "Mein Projekt"

### unsigned int `IoPiServer`::`postMessage`(`message`)
Die Methode verschickt einen HTTP-POST-Request an den Server, um diesem eine Nachricht zu schicken. Die Nachricht wird in der Ereignisanzeige des Web-Interfaces angezeigt.
- `message` Die Nachricht, die an den Server geschickt werden soll, z.B. "Hallo Server"
- Gibt bei Erfolg 0 zurück, sonst einen Error-Code.

### unsigned int `IoPiServer`::`postData`(`label`, `value`)
Die Methode verschickt einen HTTP-POST-Request um einen Zahlenwert, z.B. von einem Sensor, an den Server zu senden. Dort wird der Wert, zusammen mit anderen Werten desselben Labels, als Graph im Abschnitt Sensordaten angezeigt.
- `label` Die Bezeichnung des Datensatzes, z.B. "Temperatur". Alle Werte desselben Labels werden vom Server zu einem Datensatz zusammen gefasst und als zusammenhängender Graph angezeigt.
- `value` Der eigentliche Wert, der übertragen werden soll, z.B. 5 oder 22.6 
- Gibt bei Erfolg 0 zurück, sonst einen Error-Code.

### String `IoPiServer`::`getCommand`()
Die Methode verschickt einen HTTP-GET-Request, um beim Server anzufragen, ob ein Befehl in der Warteschlange vorliegt. Der Server antwortet entweder mit dem entpsrechdenen Befehl oder mit dem leeren String (""). 
- Die Methode gibt die Antwort des Server als String-Objekt zurück.

 