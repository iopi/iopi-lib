/*
 * Dieses Beispiel zeigt, wie eine Verbindung mit einem
 * IoPiServer hergestellt wird, um ihm danach alle 5 Sekunden eine
 * "Hallo Server!" Nachricht zu schicken.
 *
 * Vorraussetzung ist, dass eine Netzwerkverbindung zum Server existiert.
 */

#include <Bridge.h>
#include <IoPi.h>

// Das IoPiServer-Objekt:
IoPiServer server;

void setup() {
	// Initialisiere das Server-Objekt
	server.begin("http://iopi.informatik.hu-berlin.de", "Mein Projekt");
}

void loop() {

	// Sende die Nachrcht "Hallo Server!" an den Server.
	server.postMessage("Hallo Server!");

	// Warte 5 Sekunden.
	delay(5000);
}


