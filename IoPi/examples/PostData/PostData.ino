/*
 * Dieses Beispiel zeigt, wie Datenwerte (z.B. von Sensoren)
 * an einen IoPiServer geschickt werden können. In diesem Fall
 * wird anstelle des Sensorwertes eine Zufallszahl generiert.
 *
 * Vorraussetzung ist, dass eine Netzwerkverbindung zum Server existiert.
 */

#include <Bridge.h>
#include <IoPi.h>

// Das IoPiServer-Objekt:
IoPiServer server;

void setup() {
	// Initialisiere das Server-Objekt
	server.begin("http://iopi.informatik.hu-berlin.de", "Mein Projekt");
}

void loop() {

	// Generiere eine zufällige Zahl zwischen 0 und 50
	int zufall = random(50);

	// Schicke die Zahl unter dem Namen "Zufall" an den Server.
	server.postData("Zufall", zufall);

	delay(1000);
}


