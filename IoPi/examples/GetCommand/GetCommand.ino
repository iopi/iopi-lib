/*
 * Diese Beispiel zeigt, wie eine Verbindung mit einem IoPi-Server
 * hergestellt wird, um von diesem Server Befehle zu empfangen. In diesem
 * Beispiel wird eine LED an Pin 26 an- oder ausgeschaltet, je nach dem,
 * welcher Befehl empfangen wird.
 *
 * Vorraussetzung ist, dass eine Netzwerkverbindung zum Server existiert.
 */

#include <Bridge.h>
#include <IoPi.h>

// Das IoPiServer-Objekt
IoPiServer server;

// LED pin
int LED = 26;

void setup() {
	Console.begin();

	pinMode(LED, OUTPUT);
	digitalWrite(LED, LOW);

	// Initialisiere das Server-Objekt
	server.begin("http://iopi.informatik.hu-berlin.de", "Mein Projekt");
}

void loop() {

	// Schau nach, ob auf dem Server ein Befehl für uns vorliegt. Sollte kein
	// Befehl vorliegen, oder etwas schief gehen, erhalten wir einen leeren String.
	String befehl = server.getCommand();

	// Wenn wir "licht an" erhalten haben, schalte die LED an.
	if (befehl == "licht an") {
		digitalWrite(LED, HIGH);

	// Wenn wir "licht aus" erhalten haben, schalte die LED aus.
	} else if (befehl == "licht aus") {
		digitalWrite(LED, LOW);

	// Wenn wir einen anderen Befehl erhalten haben, gib ihn auf dem Monitor aus.
	} else if (befehl != "")  {
		Console.println("Unbekannter Befehl: " + befehl);
	}

	// Warte ein bisschen
	delay(500);
}

