/*
 * IoPi.h
 *
 *
 *  Created on: Aug 30, 2017
 *      Author: Michael T. Rücker
 */

#ifndef IOPI_LIB_H_
#define IOPI_LIB_H_

#include <Arduino.h>
#include <Bridge.h>
#include <HttpClient.h>

/**
 * INTERNET OF PI
 * ==============
 */

class IoPiServer {

private:
	String m_baseUrl;
	String m_project;
	HttpClient m_httpClient;

	void init(const char *url, const char *project);
	void encodeUrl(String &url);

public:
	IoPiServer() {}

	void begin(const char *url, const char *project);
	void begin(String &url, String &project);

	unsigned int postData(String &sensorName, int value);
	unsigned int postData(const char *sensorName, int value);

	unsigned int postMessage(String &event);
	unsigned int postMessage(const char *event);

	String getCommand();
};

#endif /* IOPI_LIB_H_ */
